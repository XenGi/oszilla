EESchema Schematic File Version 4
LIBS:oszilla-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R1
U 1 1 5D474C87
P 1850 1250
F 0 "R1" H 1920 1296 50  0000 L CNN
F 1 "1K" H 1920 1205 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 1780 1250 50  0001 C CNN
F 3 "~" H 1850 1250 50  0001 C CNN
	1    1850 1250
	1    0    0    -1  
$EndComp
$Comp
L Device:R_POT RV1
U 1 1 5D4762DC
P 1850 1650
F 0 "RV1" H 1781 1696 50  0000 R CNN
F 1 "10K" H 1781 1605 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_Alps_RK09K_Single_Vertical" H 1850 1650 50  0001 C CNN
F 3 "RK09K1130A8G" H 1850 1650 50  0001 C CNN
	1    1850 1650
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 5D476C9C
P 2300 2300
F 0 "R5" H 2230 2254 50  0000 R CNN
F 1 "100K" H 2230 2345 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 2230 2300 50  0001 C CNN
F 3 "~" H 2300 2300 50  0001 C CNN
	1    2300 2300
	-1   0    0    1   
$EndComp
$Comp
L Device:CP C1
U 1 1 5D477799
P 950 2350
F 0 "C1" H 1068 2396 50  0000 L CNN
F 1 "1uF" H 1068 2305 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 988 2200 50  0001 C CNN
F 3 "~" H 950 2350 50  0001 C CNN
	1    950  2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 2050 1850 2100
Connection ~ 1850 2100
NoConn ~ 2150 2350
Wire Wire Line
	950  2100 950  2200
Wire Wire Line
	950  2500 950  2600
Wire Wire Line
	1850 2100 1850 2150
$Comp
L Device:LED D1
U 1 1 5D47F3A4
P 1850 2800
F 0 "D1" V 1889 2683 50  0000 R CNN
F 1 "LED" V 1798 2683 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm_Clear" H 1850 2800 50  0001 C CNN
F 3 "~" H 1850 2800 50  0001 C CNN
	1    1850 2800
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR02
U 1 1 5D47FFE9
P 1850 3000
F 0 "#PWR02" H 1850 2750 50  0001 C CNN
F 1 "GND" H 1855 2827 50  0000 C CNN
F 2 "" H 1850 3000 50  0001 C CNN
F 3 "" H 1850 3000 50  0001 C CNN
	1    1850 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 3000 1850 2950
Wire Wire Line
	1850 1050 1850 1100
Text GLabel 2300 2500 3    50   Input ~ 0
AUDIO_OUT
$Comp
L Device:Q_NPN_EBC Q1
U 1 1 5D482DD2
P 1950 2350
F 0 "Q1" H 2141 2304 50  0000 L CNN
F 1 "SS9018HBU" H 2141 2395 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_HandSolder" H 2150 2450 50  0001 C CNN
F 3 "~" H 1950 2350 50  0001 C CNN
	1    1950 2350
	-1   0    0    1   
$EndComp
Wire Wire Line
	1850 2550 1850 2600
Wire Wire Line
	950  2100 1850 2100
Wire Wire Line
	950  2600 1850 2600
Connection ~ 1850 2600
Wire Wire Line
	1850 2600 1850 2650
$Comp
L Device:R R2
U 1 1 5D48554A
P 3850 1250
F 0 "R2" H 3920 1296 50  0000 L CNN
F 1 "1K" H 3920 1205 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3780 1250 50  0001 C CNN
F 3 "~" H 3850 1250 50  0001 C CNN
	1    3850 1250
	1    0    0    -1  
$EndComp
$Comp
L Device:R_POT RV2
U 1 1 5D485554
P 3850 1650
F 0 "RV2" H 3781 1696 50  0000 R CNN
F 1 "10K" H 3781 1605 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_Alps_RK09K_Single_Vertical" H 3850 1650 50  0001 C CNN
F 3 "RK09K1130A8G" H 3850 1650 50  0001 C CNN
	1    3850 1650
	1    0    0    -1  
$EndComp
$Comp
L Device:R R6
U 1 1 5D48555E
P 4300 2300
F 0 "R6" H 4230 2254 50  0000 R CNN
F 1 "100K" H 4230 2345 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4230 2300 50  0001 C CNN
F 3 "~" H 4300 2300 50  0001 C CNN
	1    4300 2300
	-1   0    0    1   
$EndComp
$Comp
L Device:CP C2
U 1 1 5D485568
P 2950 2350
F 0 "C2" H 3068 2396 50  0000 L CNN
F 1 "10uF" H 3068 2305 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 2988 2200 50  0001 C CNN
F 3 "~" H 2950 2350 50  0001 C CNN
	1    2950 2350
	1    0    0    -1  
$EndComp
Connection ~ 3850 2100
NoConn ~ 4150 2350
Wire Wire Line
	2950 2100 2950 2200
Wire Wire Line
	2950 2500 2950 2600
Wire Wire Line
	3850 2100 3850 2150
$Comp
L Device:LED D2
U 1 1 5D485578
P 3850 2800
F 0 "D2" V 3889 2683 50  0000 R CNN
F 1 "LED" V 3798 2683 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm_Clear" H 3850 2800 50  0001 C CNN
F 3 "~" H 3850 2800 50  0001 C CNN
	1    3850 2800
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 5D485582
P 3850 3000
F 0 "#PWR0101" H 3850 2750 50  0001 C CNN
F 1 "GND" H 3855 2827 50  0000 C CNN
F 2 "" H 3850 3000 50  0001 C CNN
F 3 "" H 3850 3000 50  0001 C CNN
	1    3850 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 3000 3850 2950
Wire Wire Line
	3850 1050 3850 1100
Text GLabel 4300 2500 3    50   Input ~ 0
AUDIO_OUT
Wire Wire Line
	4300 2500 4300 2450
$Comp
L Device:Q_NPN_EBC Q2
U 1 1 5D4855A0
P 3950 2350
F 0 "Q2" H 4141 2304 50  0000 L CNN
F 1 "SS9018HBU" H 4141 2395 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_HandSolder" H 4150 2450 50  0001 C CNN
F 3 "~" H 3950 2350 50  0001 C CNN
	1    3950 2350
	-1   0    0    1   
$EndComp
Wire Wire Line
	3850 2550 3850 2600
Wire Wire Line
	2950 2100 3850 2100
Wire Wire Line
	2950 2600 3850 2600
Connection ~ 3850 2600
Wire Wire Line
	3850 2600 3850 2650
$Comp
L Device:R R3
U 1 1 5D488F3E
P 5850 1250
F 0 "R3" H 5920 1296 50  0000 L CNN
F 1 "1K" H 5920 1205 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5780 1250 50  0001 C CNN
F 3 "~" H 5850 1250 50  0001 C CNN
	1    5850 1250
	1    0    0    -1  
$EndComp
$Comp
L Device:R_POT RV3
U 1 1 5D488F48
P 5850 1650
F 0 "RV3" H 5781 1696 50  0000 R CNN
F 1 "10K" H 5781 1605 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_Alps_RK09K_Single_Vertical" H 5850 1650 50  0001 C CNN
F 3 "RK09K1130A8G" H 5850 1650 50  0001 C CNN
	1    5850 1650
	1    0    0    -1  
$EndComp
$Comp
L Device:R R7
U 1 1 5D488F52
P 6300 2300
F 0 "R7" H 6230 2254 50  0000 R CNN
F 1 "100K" H 6230 2345 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 6230 2300 50  0001 C CNN
F 3 "~" H 6300 2300 50  0001 C CNN
	1    6300 2300
	-1   0    0    1   
$EndComp
$Comp
L Device:CP C3
U 1 1 5D488F5C
P 4950 2350
F 0 "C3" H 5068 2396 50  0000 L CNN
F 1 "47uF" H 5068 2305 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 4988 2200 50  0001 C CNN
F 3 "~" H 4950 2350 50  0001 C CNN
	1    4950 2350
	1    0    0    -1  
$EndComp
Connection ~ 5850 2100
NoConn ~ 6150 2350
Wire Wire Line
	4950 2100 4950 2200
Wire Wire Line
	4950 2500 4950 2600
Wire Wire Line
	5850 2100 5850 2150
$Comp
L Device:LED D3
U 1 1 5D488F6C
P 5850 2800
F 0 "D3" V 5889 2683 50  0000 R CNN
F 1 "LED" V 5798 2683 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm_Clear" H 5850 2800 50  0001 C CNN
F 3 "~" H 5850 2800 50  0001 C CNN
	1    5850 2800
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 5D488F76
P 5850 3000
F 0 "#PWR0103" H 5850 2750 50  0001 C CNN
F 1 "GND" H 5855 2827 50  0000 C CNN
F 2 "" H 5850 3000 50  0001 C CNN
F 3 "" H 5850 3000 50  0001 C CNN
	1    5850 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5850 3000 5850 2950
Wire Wire Line
	5850 1050 5850 1100
Text GLabel 6300 2500 3    50   Input ~ 0
AUDIO_OUT
Wire Wire Line
	6300 2500 6300 2450
$Comp
L Device:Q_NPN_EBC Q3
U 1 1 5D488F94
P 5950 2350
F 0 "Q3" H 6141 2304 50  0000 L CNN
F 1 "SS9018HBU" H 6141 2395 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_HandSolder" H 6150 2450 50  0001 C CNN
F 3 "~" H 5950 2350 50  0001 C CNN
	1    5950 2350
	-1   0    0    1   
$EndComp
Wire Wire Line
	5850 2550 5850 2600
Wire Wire Line
	4950 2100 5850 2100
Wire Wire Line
	4950 2600 5850 2600
Connection ~ 5850 2600
Wire Wire Line
	5850 2600 5850 2650
$Comp
L Device:R R4
U 1 1 5D49335E
P 7850 1250
F 0 "R4" H 7920 1296 50  0000 L CNN
F 1 "1K" H 7920 1205 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 7780 1250 50  0001 C CNN
F 3 "~" H 7850 1250 50  0001 C CNN
	1    7850 1250
	1    0    0    -1  
$EndComp
$Comp
L Device:R_POT RV4
U 1 1 5D493368
P 7850 1650
F 0 "RV4" H 7781 1696 50  0000 R CNN
F 1 "10K" H 7781 1605 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_Alps_RK09K_Single_Vertical" H 7850 1650 50  0001 C CNN
F 3 "RK09K1130A8G" H 7850 1650 50  0001 C CNN
	1    7850 1650
	1    0    0    -1  
$EndComp
$Comp
L Device:R R8
U 1 1 5D493372
P 8300 2300
F 0 "R8" H 8230 2254 50  0000 R CNN
F 1 "100K" H 8230 2345 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 8230 2300 50  0001 C CNN
F 3 "~" H 8300 2300 50  0001 C CNN
	1    8300 2300
	-1   0    0    1   
$EndComp
$Comp
L Device:CP C4
U 1 1 5D49337C
P 6950 2350
F 0 "C4" H 7068 2396 50  0000 L CNN
F 1 "100uF" H 7068 2305 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 6988 2200 50  0001 C CNN
F 3 "~" H 6950 2350 50  0001 C CNN
	1    6950 2350
	1    0    0    -1  
$EndComp
Connection ~ 7850 2100
NoConn ~ 8150 2350
Wire Wire Line
	6950 2100 6950 2200
Wire Wire Line
	6950 2500 6950 2600
Wire Wire Line
	7850 2100 7850 2150
$Comp
L Device:LED D4
U 1 1 5D49338C
P 7850 2800
F 0 "D4" V 7889 2683 50  0000 R CNN
F 1 "LED" V 7798 2683 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm_Clear" H 7850 2800 50  0001 C CNN
F 3 "~" H 7850 2800 50  0001 C CNN
	1    7850 2800
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 5D493396
P 7850 3000
F 0 "#PWR0105" H 7850 2750 50  0001 C CNN
F 1 "GND" H 7855 2827 50  0000 C CNN
F 2 "" H 7850 3000 50  0001 C CNN
F 3 "" H 7850 3000 50  0001 C CNN
	1    7850 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	7850 3000 7850 2950
Wire Wire Line
	7850 1050 7850 1100
Text GLabel 8300 2500 3    50   Input ~ 0
AUDIO_OUT
Wire Wire Line
	8300 2500 8300 2450
$Comp
L Device:Q_NPN_EBC Q4
U 1 1 5D4933B4
P 7950 2350
F 0 "Q4" H 8141 2304 50  0000 L CNN
F 1 "SS9018HBU" H 8141 2395 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_HandSolder" H 8150 2450 50  0001 C CNN
F 3 "~" H 7950 2350 50  0001 C CNN
	1    7950 2350
	-1   0    0    1   
$EndComp
Wire Wire Line
	7850 2550 7850 2600
Wire Wire Line
	6950 2100 7850 2100
Wire Wire Line
	6950 2600 7850 2600
Connection ~ 7850 2600
Wire Wire Line
	7850 2600 7850 2650
$Comp
L Device:LED D5
U 1 1 5D4A4F35
P 2200 3850
F 0 "D5" V 2147 3928 50  0000 L CNN
F 1 "LED" V 2238 3928 50  0000 L CNN
F 2 "LED_THT:LED_D5.0mm_Clear" H 2200 3850 50  0001 C CNN
F 3 "~" H 2200 3850 50  0001 C CNN
	1    2200 3850
	-1   0    0    1   
$EndComp
$Comp
L Device:R R9
U 1 1 5D4B364B
P 2200 3550
F 0 "R9" H 2270 3596 50  0000 L CNN
F 1 "1K" H 2270 3505 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 2130 3550 50  0001 C CNN
F 3 "~" H 2200 3550 50  0001 C CNN
	1    2200 3550
	0    1    1    0   
$EndComp
Connection ~ 1850 2050
Wire Wire Line
	2300 2450 2300 2500
$Comp
L Device:R_PHOTO R13
U 1 1 5D510D89
P 2100 1850
F 0 "R13" H 2170 1896 50  0000 L CNN
F 1 "R_PHOTO" H 2170 1805 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P2.54mm_Vertical" V 2150 1600 50  0001 L CNN
F 3 "~" H 2100 1800 50  0001 C CNN
	1    2100 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 1700 2100 1650
Wire Wire Line
	2100 1650 2000 1650
Wire Wire Line
	1850 1800 1850 2050
Wire Wire Line
	2100 2000 2100 2050
Wire Wire Line
	1850 2050 2100 2050
Wire Wire Line
	1850 2100 2300 2100
Wire Wire Line
	2300 2150 2300 2100
Wire Wire Line
	4300 2150 4300 2100
Wire Wire Line
	3850 2100 4300 2100
$Comp
L Device:R_PHOTO R14
U 1 1 5D52A492
P 4100 1850
F 0 "R14" H 4170 1896 50  0000 L CNN
F 1 "R_PHOTO" H 4170 1805 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P2.54mm_Vertical" V 4150 1600 50  0001 L CNN
F 3 "~" H 4100 1800 50  0001 C CNN
	1    4100 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 1800 3850 2050
Wire Wire Line
	3850 2050 4100 2050
Wire Wire Line
	4100 2050 4100 2000
Connection ~ 3850 2050
Wire Wire Line
	3850 2050 3850 2100
Wire Wire Line
	4000 1650 4100 1650
Wire Wire Line
	4100 1650 4100 1700
Wire Wire Line
	3850 1450 3850 1500
$Comp
L Device:R_PHOTO R15
U 1 1 5D54DC0E
P 6100 1850
F 0 "R15" H 6170 1896 50  0000 L CNN
F 1 "R_PHOTO" H 6170 1805 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P2.54mm_Vertical" V 6150 1600 50  0001 L CNN
F 3 "~" H 6100 1800 50  0001 C CNN
	1    6100 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	6300 2150 6300 2100
Wire Wire Line
	5850 2100 6300 2100
Wire Wire Line
	5850 1800 5850 2050
Wire Wire Line
	6000 1650 6100 1650
Wire Wire Line
	6100 1650 6100 1700
Wire Wire Line
	6100 2000 6100 2050
Wire Wire Line
	6100 2050 5850 2050
Connection ~ 5850 2050
Wire Wire Line
	5850 2050 5850 2100
$Comp
L Device:R_PHOTO R16
U 1 1 5D56F0F5
P 8100 1850
F 0 "R16" H 8170 1896 50  0000 L CNN
F 1 "R_PHOTO" H 8170 1805 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P2.54mm_Vertical" V 8150 1600 50  0001 L CNN
F 3 "~" H 8100 1800 50  0001 C CNN
	1    8100 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	8300 2150 8300 2100
Wire Wire Line
	7850 2100 8300 2100
Wire Wire Line
	7850 1450 7850 1500
Wire Wire Line
	8000 1650 8100 1650
Wire Wire Line
	8100 1650 8100 1700
Wire Wire Line
	7850 1800 7850 2050
Wire Wire Line
	7850 2050 8100 2050
Wire Wire Line
	8100 2050 8100 2000
Connection ~ 7850 2050
Wire Wire Line
	7850 2050 7850 2100
Wire Wire Line
	1850 1400 1850 1450
Wire Wire Line
	1850 1450 2100 1450
Wire Wire Line
	2100 1450 2100 1650
Connection ~ 1850 1450
Wire Wire Line
	1850 1450 1850 1500
Connection ~ 2100 1650
Wire Wire Line
	3850 1450 4100 1450
Wire Wire Line
	4100 1450 4100 1650
Connection ~ 4100 1650
Wire Wire Line
	3850 1400 3850 1450
Connection ~ 3850 1450
Wire Wire Line
	5850 1400 5850 1450
Wire Wire Line
	6100 1650 6100 1450
Wire Wire Line
	6100 1450 5850 1450
Connection ~ 6100 1650
Connection ~ 5850 1450
Wire Wire Line
	5850 1450 5850 1500
Wire Wire Line
	8100 1650 8100 1450
Wire Wire Line
	8100 1450 7850 1450
Connection ~ 8100 1650
Wire Wire Line
	7850 1450 7850 1400
Connection ~ 7850 1450
$Comp
L power:+9V #PWR0102
U 1 1 5D5DEF63
P 1850 1050
F 0 "#PWR0102" H 1850 900 50  0001 C CNN
F 1 "+9V" H 1865 1223 50  0000 C CNN
F 2 "" H 1850 1050 50  0001 C CNN
F 3 "" H 1850 1050 50  0001 C CNN
	1    1850 1050
	1    0    0    -1  
$EndComp
$Comp
L power:+9V #PWR0104
U 1 1 5D5DFAB0
P 3850 1050
F 0 "#PWR0104" H 3850 900 50  0001 C CNN
F 1 "+9V" H 3865 1223 50  0000 C CNN
F 2 "" H 3850 1050 50  0001 C CNN
F 3 "" H 3850 1050 50  0001 C CNN
	1    3850 1050
	1    0    0    -1  
$EndComp
$Comp
L power:+9V #PWR0106
U 1 1 5D5E012F
P 5850 1050
F 0 "#PWR0106" H 5850 900 50  0001 C CNN
F 1 "+9V" H 5865 1223 50  0000 C CNN
F 2 "" H 5850 1050 50  0001 C CNN
F 3 "" H 5850 1050 50  0001 C CNN
	1    5850 1050
	1    0    0    -1  
$EndComp
$Comp
L power:+9V #PWR0108
U 1 1 5D5E03BE
P 7850 1050
F 0 "#PWR0108" H 7850 900 50  0001 C CNN
F 1 "+9V" H 7865 1223 50  0000 C CNN
F 2 "" H 7850 1050 50  0001 C CNN
F 3 "" H 7850 1050 50  0001 C CNN
	1    7850 1050
	1    0    0    -1  
$EndComp
$Comp
L Connector:AudioJack3 J1
U 1 1 5D47D97B
P 1750 3650
F 0 "J1" H 1732 3975 50  0000 C CNN
F 1 "AudioJack3" H 1732 3884 50  0000 C CNN
F 2 "Connector_Audio:Jack_3.5mm_CUI_SJ1-3533NG_Horizontal" H 1750 3650 50  0001 C CNN
F 3 "~" H 1750 3650 50  0001 C CNN
	1    1750 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	1950 3650 2000 3650
Wire Wire Line
	1950 3750 2000 3750
Wire Wire Line
	2000 3750 2000 3850
Wire Wire Line
	2000 3850 2050 3850
Wire Wire Line
	2350 3550 2400 3550
Wire Wire Line
	2400 3550 2400 3850
Wire Wire Line
	2400 3850 2350 3850
Wire Wire Line
	1950 3550 2050 3550
Wire Wire Line
	2000 3650 2000 3750
Connection ~ 2000 3750
$Comp
L Device:LED D6
U 1 1 5D4B03DD
P 4200 3850
F 0 "D6" V 4147 3928 50  0000 L CNN
F 1 "LED" V 4238 3928 50  0000 L CNN
F 2 "LED_THT:LED_D5.0mm_Clear" H 4200 3850 50  0001 C CNN
F 3 "~" H 4200 3850 50  0001 C CNN
	1    4200 3850
	-1   0    0    1   
$EndComp
$Comp
L Device:R R10
U 1 1 5D4B03E7
P 4200 3550
F 0 "R10" H 4270 3596 50  0000 L CNN
F 1 "1K" H 4270 3505 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4130 3550 50  0001 C CNN
F 3 "~" H 4200 3550 50  0001 C CNN
	1    4200 3550
	0    1    1    0   
$EndComp
$Comp
L Connector:AudioJack3 J2
U 1 1 5D4B03F1
P 3750 3650
F 0 "J2" H 3732 3975 50  0000 C CNN
F 1 "AudioJack3" H 3732 3884 50  0000 C CNN
F 2 "Connector_Audio:Jack_3.5mm_CUI_SJ1-3533NG_Horizontal" H 3750 3650 50  0001 C CNN
F 3 "~" H 3750 3650 50  0001 C CNN
	1    3750 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 3650 4000 3650
Wire Wire Line
	3950 3750 4000 3750
Wire Wire Line
	4000 3750 4000 3850
Wire Wire Line
	4000 3850 4050 3850
Wire Wire Line
	4350 3550 4400 3550
Wire Wire Line
	4400 3550 4400 3850
Wire Wire Line
	4400 3850 4350 3850
Wire Wire Line
	3950 3550 4050 3550
Wire Wire Line
	4000 3650 4000 3750
Connection ~ 4000 3750
$Comp
L Device:LED D7
U 1 1 5D4C0C2C
P 6200 3850
F 0 "D7" V 6147 3928 50  0000 L CNN
F 1 "LED" V 6238 3928 50  0000 L CNN
F 2 "LED_THT:LED_D5.0mm_Clear" H 6200 3850 50  0001 C CNN
F 3 "~" H 6200 3850 50  0001 C CNN
	1    6200 3850
	-1   0    0    1   
$EndComp
$Comp
L Device:R R11
U 1 1 5D4C0C36
P 6200 3550
F 0 "R11" H 6270 3596 50  0000 L CNN
F 1 "1K" H 6270 3505 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 6130 3550 50  0001 C CNN
F 3 "~" H 6200 3550 50  0001 C CNN
	1    6200 3550
	0    1    1    0   
$EndComp
$Comp
L Connector:AudioJack3 J3
U 1 1 5D4C0C40
P 5750 3650
F 0 "J3" H 5732 3975 50  0000 C CNN
F 1 "AudioJack3" H 5732 3884 50  0000 C CNN
F 2 "Connector_Audio:Jack_3.5mm_CUI_SJ1-3533NG_Horizontal" H 5750 3650 50  0001 C CNN
F 3 "~" H 5750 3650 50  0001 C CNN
	1    5750 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 3650 6000 3650
Wire Wire Line
	5950 3750 6000 3750
Wire Wire Line
	6000 3750 6000 3850
Wire Wire Line
	6000 3850 6050 3850
Wire Wire Line
	6350 3550 6400 3550
Wire Wire Line
	6400 3550 6400 3850
Wire Wire Line
	6400 3850 6350 3850
Wire Wire Line
	5950 3550 6050 3550
Wire Wire Line
	6000 3650 6000 3750
Connection ~ 6000 3750
$Comp
L Device:LED D8
U 1 1 5D4C8918
P 8200 3850
F 0 "D8" V 8147 3928 50  0000 L CNN
F 1 "LED" V 8238 3928 50  0000 L CNN
F 2 "LED_THT:LED_D5.0mm_Clear" H 8200 3850 50  0001 C CNN
F 3 "~" H 8200 3850 50  0001 C CNN
	1    8200 3850
	-1   0    0    1   
$EndComp
$Comp
L Device:R R12
U 1 1 5D4C8922
P 8200 3550
F 0 "R12" H 8270 3596 50  0000 L CNN
F 1 "1K" H 8270 3505 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 8130 3550 50  0001 C CNN
F 3 "~" H 8200 3550 50  0001 C CNN
	1    8200 3550
	0    1    1    0   
$EndComp
$Comp
L Connector:AudioJack3 J4
U 1 1 5D4C892C
P 7750 3650
F 0 "J4" H 7732 3975 50  0000 C CNN
F 1 "AudioJack3" H 7732 3884 50  0000 C CNN
F 2 "Connector_Audio:Jack_3.5mm_CUI_SJ1-3533NG_Horizontal" H 7750 3650 50  0001 C CNN
F 3 "~" H 7750 3650 50  0001 C CNN
	1    7750 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	7950 3650 8000 3650
Wire Wire Line
	7950 3750 8000 3750
Wire Wire Line
	8000 3750 8000 3850
Wire Wire Line
	8000 3850 8050 3850
Wire Wire Line
	8350 3550 8400 3550
Wire Wire Line
	8400 3550 8400 3850
Wire Wire Line
	8400 3850 8350 3850
Wire Wire Line
	7950 3550 8050 3550
Wire Wire Line
	8000 3650 8000 3750
Connection ~ 8000 3750
$Comp
L Connector:AudioJack3 J5
U 1 1 5D4D186B
P 9750 2400
F 0 "J5" H 9470 2333 50  0000 R CNN
F 1 "AudioJack3" H 9470 2424 50  0000 R CNN
F 2 "Connector_Audio:Jack_3.5mm_CUI_SJ1-3533NG_Horizontal" H 9750 2400 50  0001 C CNN
F 3 "~" H 9750 2400 50  0001 C CNN
	1    9750 2400
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 5D524836
P 9100 2750
F 0 "#PWR0107" H 9100 2500 50  0001 C CNN
F 1 "GND" H 9105 2577 50  0000 C CNN
F 2 "" H 9100 2750 50  0001 C CNN
F 3 "" H 9100 2750 50  0001 C CNN
	1    9100 2750
	1    0    0    -1  
$EndComp
Text GLabel 9500 2100 1    50   Input ~ 0
AUDIO_OUT
$Comp
L Device:R_POT RV5
U 1 1 5D539583
P 9300 2300
F 0 "RV5" H 9230 2254 50  0000 R CNN
F 1 "100K" H 9230 2345 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_Alps_RK09K_Single_Vertical" H 9300 2300 50  0001 C CNN
F 3 "RK09K1130A8G" H 9300 2300 50  0001 C CNN
	1    9300 2300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9300 2150 9300 2100
Wire Wire Line
	9300 2100 9100 2100
Wire Wire Line
	9100 2100 9100 2300
Wire Wire Line
	9100 2300 9150 2300
$Comp
L Device:C C5
U 1 1 5D545B7B
P 9100 2500
F 0 "C5" H 8850 2550 50  0000 L CNN
F 1 "470uF" H 8750 2450 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 9138 2350 50  0001 C CNN
F 3 "~" H 9100 2500 50  0001 C CNN
	1    9100 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	9100 2650 9100 2700
Wire Wire Line
	9550 2500 9500 2500
Wire Wire Line
	9500 2500 9500 2700
Wire Wire Line
	9500 2700 9100 2700
Connection ~ 9100 2700
Wire Wire Line
	9100 2700 9100 2750
Wire Wire Line
	9450 2300 9500 2300
Wire Wire Line
	9500 2100 9500 2300
Connection ~ 9500 2300
Wire Wire Line
	9500 2300 9550 2300
Wire Wire Line
	9500 2300 9500 2400
Wire Wire Line
	9500 2400 9550 2400
Wire Wire Line
	9100 2350 9100 2300
Connection ~ 9100 2300
$Comp
L power:GND #PWR0109
U 1 1 5D5EB47E
P 9250 3650
F 0 "#PWR0109" H 9250 3400 50  0001 C CNN
F 1 "GND" H 9255 3477 50  0000 C CNN
F 2 "" H 9250 3650 50  0001 C CNN
F 3 "" H 9250 3650 50  0001 C CNN
	1    9250 3650
	1    0    0    -1  
$EndComp
$Comp
L power:+9V #PWR0110
U 1 1 5D5EBCDA
P 9250 3450
F 0 "#PWR0110" H 9250 3300 50  0001 C CNN
F 1 "+9V" H 9265 3623 50  0000 C CNN
F 2 "" H 9250 3450 50  0001 C CNN
F 3 "" H 9250 3450 50  0001 C CNN
	1    9250 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	9250 3450 9250 3500
Wire Wire Line
	9250 3500 9300 3500
Wire Wire Line
	9300 3600 9250 3600
Wire Wire Line
	9250 3600 9250 3650
$Comp
L Connector:Conn_01x02_Male J6
U 1 1 5D5F81E9
P 9500 3600
F 0 "J6" H 9472 3482 50  0000 R CNN
F 1 "Conn_01x02_Male" H 9472 3573 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 9500 3600 50  0001 C CNN
F 3 "~" H 9500 3600 50  0001 C CNN
	1    9500 3600
	-1   0    0    1   
$EndComp
$EndSCHEMATC
