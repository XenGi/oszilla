![oszilla pcb][oszilla_pcb]

# oszilla

godzilla of oscillators!

Based on the design of [LOOK MUM NO COMPUTER][lmnc].

---

[![Creative Commons License](https://i.creativecommons.org/l/by/4.0/88x31.png)][ccby]

This work is licensed under a [Creative Commons Attribution 4.0 International License][ccby].


[oszilla_pcb]: https://gitlab.com/XenGi/oszilla/raw/master/oszilla.png
[lmnc]: https://www.lookmumnocomputer.com/simplest-oscillator/
[ccby]: http://creativecommons.org/licenses/by/4.0/
