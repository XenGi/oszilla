BOM
===

| Part    | Qty | Description         |
| ------- | --- | ------------------- |
| R1-R4   |   4 | 1K Resistor         |
| R5-R8   |   4 | 100K Resistor       |
| R9-R12  |   4 | 1K Resistor         |
| R13-R16 |   4 | 5-10K Photoresistor |
| RV1-RV4 |   4 | 10K Potentiometers  |
| RV5     |   1 | 100K Potentiometers |
| Q1-Q4   |   4 | SS9018 Resistor     |
| C1      |   1 | 1uF Capacitor       |
| C2      |   1 | 10uF Capacitor      |
| C3      |   1 | 47uF Capacitor      |
| C4      |   1 | 100uF Capacitor     |
| C5      |   1 | 47nF Capacitor      |
| D1      |   1 | Red LED             |
| D2      |   1 | Green LED           |
| D3      |   1 | Blue LED            |
| D4      |   1 | Yellow LED          |
| D5-D8   |   4 | Bright White LED    |
| J1-J5   |   5 | Audiojack           |

Get it on [Octopart][octopart].
Get the photo resistor on [ebay][ebay].


[octopart]: https://octopart.com/bom-tool/pLHIx23L
[ebay]: https://www.ebay.com/sch/i.html?_nkw=photoresitor+5+-+10k
